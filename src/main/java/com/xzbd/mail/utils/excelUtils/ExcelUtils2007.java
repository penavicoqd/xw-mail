package com.xzbd.mail.utils.excelUtils;

import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.function.Function;

import com.mysql.fabric.xmlrpc.base.Array;
import com.xzbd.system.domain.DeptDO;
import com.xzbd.system.domain.UserDO;

import org.apache.poi.ss.formula.functions.T;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;

public class ExcelUtils2007 {

    /**
     * 创建工作表
     *
     * @return
     */
    public static SXSSFWorkbook createXSSFWorkbook(int initSize) {
        SXSSFWorkbook workbook = new SXSSFWorkbook(initSize);
        return workbook;
    }

    /**
     * 创建制定名称的sheet
     *
     * @param workbook
     * @param sheetName
     * @return
     */
    public static Sheet createSheet(SXSSFWorkbook workbook, String sheetName) {
        return workbook.createSheet(sheetName);
    }

    /**
     * 不指定sheet名称
     *
     * @param workbook
     * @return
     */
    public static Sheet createSheet(SXSSFWorkbook workbook) {
        return workbook.createSheet();
    }

    /**
     * 将Excel文件保存到指定目录
     *
     * @param workbook
     * @param filePath
     * @throws IOException
     */
    public static void writeFileInTempDir(Workbook workbook, String filePath) throws IOException {
        FileOutputStream fout = null;
        // 新建一输出流
        try {
            fout = new FileOutputStream(filePath);
            // 存盘
            workbook.write(fout);
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (Objects.nonNull(fout)) {
                // 结束关闭
                fout.flush();
                fout.close();
            }
        }

    }

    /**
     * 在指定行创建列名
     *
     * @param sheet
     * @param titleName 列名
     * @param rowNum    指定行
     * @return
     */
    public static void setColumTitle(Sheet sheet, String[] titleName, int rowNum) {
        if (Objects.isNull(titleName))
            throw new RuntimeException("列名不能为空，您传入的列名为： " + titleName);
        int cellNum = titleName.length;
        Row row = createRow(sheet, rowNum, cellNum);
        for (int i = 0; i < cellNum; i++) {
            row.getCell(i).setCellValue(setCellValue(titleName[i]));
        }
    }

    /**
     * 创建行
     *
     * @param appSheet     当前sheet
     * @param curRowNum    要创建的行的行号
     * @param cellNumInRow 要创建行的单元格
     * @return
     */
    public static Row createRow(Sheet appSheet, int curRowNum, int cellNumInRow) {
        if (Objects.isNull(appSheet))
            return null;
        Row row = appSheet.createRow(curRowNum);
        for (int i = 0; i < cellNumInRow; i++) {
            row.createCell(i);
        }
        return row;
    }

    /**
     * 设置单元格数据----基本对象转字符串
     *
     * @param value
     * @return
     */
    public static String setCellValue(Object value) {
        return Objects.isNull(value) ? "" : value.toString();
    }

    /**
     * 设置单元格数据
     *
     * @param value
     * @return
     */
    public static void setCellValue(Cell cell, Object value) {
        cell.setCellValue(Objects.isNull(value) ? "" : value.toString());
    }

    /**
     * 设置单元格数据----Integer数据处理
     *
     * @param value
     * @return
     */
    public static Integer setCellValue(Integer value) {
        return Objects.isNull(value) ? 0 : value;
    }


    public static <E> List<String> toCellData(E t,Function<E ,List<String>> fun){
        return fun.apply(t);
    }
    
    public static void main(String[] args) throws IOException {
        UserDO user = new UserDO();
        user.setBirth(new Date());
        user.setCity("北京");
        user.setDeptId(78979L);
        user.setDeptName("deptName");
        user.setUsername("username");
        Function<UserDO,List<String>> fun = obj->{
            List<String> list = new ArrayList<>();
            list.add(obj.getCity());
            list.add(obj.getBirth().toString());
            list.add(obj.getDeptName());
            list.add(obj.getDeptId().toString());
            list.add(obj.getUsername());
            return list;
        };
        List<String> s = toCellData(user, fun);
        System.out.println(s.toString()+"ssssssssssssssssssssssss");
        DeptDO do1 = new DeptDO();
        do1.setDelFlag(12);
        do1.setDeptId(2345L);
        do1.setName("name");
        do1.setOrderNum(23453);

        Function<DeptDO,List<String>> f = obj->{
            List<String> list = new ArrayList<>();
            list.add(obj.getName());
            list.add(obj.getDelFlag().toString());
            list.add(obj.getDeptId().toString());
            list.add(obj.getOrderNum().toString());
            return list;
        };
        System.out.println("dddddddddddddd");
        System.out.println(toCellData(do1, f));

    }

}
